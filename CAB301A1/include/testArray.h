#ifndef TESTARRAY_H
#define TESTARRAY_H
#include "TestArray.h"
#include <stdlib.h>
#include <time.h>
#include <windows.h>
#include <iostream>
#include <chrono>
#include <random>

class TestArray
{
    public:
        TestArray(int64_t arrayLength);
        virtual ~TestArray();
        int64_t arrayLength;
        int64_t * testArray;
        int64_t getIndexValue(int64_t index);
        typedef std::chrono::high_resolution_clock myclock;
        myclock::time_point beginning = myclock::now();

    private:
        int generateArrayintegers(int64_t arrayLength, std::mt19937 generator);
        int64_t fillArray(int64_t testArray[], int64_t arrayLength);

};

#endif // TESTARRAY_H
