#include <iostream>
#include <fstream>
#include <string>
#include <math.h>
#include <time.h>
#include <chrono>
#include <random>

using namespace std;

std::mt19937 generator;


void performAvgCountTest();
void performAvgTimeTest();
void outputTestArray();
void fillArray(int testArray[], int arrayLength);
int64_t findMedianCountTest(int testArray[], int64_t arrayLength);
clock_t findMedianTimeTest(int testArray[], int64_t arrayLength);
void outputResultToFile(int64_t arrayLength, int64_t outputData, char* resultFile);

int main(){
    //Initialize the PRNG for creating the arrays
    typedef std::chrono::high_resolution_clock myclock;
    myclock::time_point beginning = myclock::now();
    myclock::duration d = myclock::now() - beginning;
    unsigned seed = d.count();
    std::mt19937 generator(seed);

    // Perform Tests
    outputTestArray();  //Functional testing
    performAvgCountTest(); // Average number of operations test
    performAvgTimeTest(); // Average execution time test
    cout<<'\a'; //Beep on completetion
    return 0;
}

void performAvgCountTest(){
    // Create results file
    char* resultFile = "avgcountresults.csv";
    ofstream outputFile;
    outputFile.open(resultFile, ios::trunc);
    outputFile<<"n,OpCount\n";
    outputFile.close();

    // Inititialize variables for testing
    int64_t maxArrayLength = 2560; //largest n to test
    int64_t minArrayLength = 10;   //Smallest n to test
    int64_t arrayStepSize = 100;   //Array size increment
    int64_t numTestsToAvg = 250;   //Number of tests to perform per size

    int64_t currentArrayLength = minArrayLength;

    // Run tests
    for (currentArrayLength; currentArrayLength <= maxArrayLength; currentArrayLength += arrayStepSize){
            int64_t opCount = 0;

            for (int64_t test = 0; test<numTestsToAvg; test++){
                int *testArray = new int[currentArrayLength]; // Create test array
                fillArray(testArray, currentArrayLength);  // Fill array with random numbers

                //run search and return operations count
                opCount += findMedianCountTest(testArray, currentArrayLength);

                delete testArray;
                }
            //Calculate average and output to results file
            opCount = opCount/numTestsToAvg;
            outputResultToFile(currentArrayLength, opCount, resultFile);
    }
}


void performAvgTimeTest() {
    // Create results file
    char* resultFile = "avgtimeresults.csv";
    ofstream outputFile;
    outputFile.open(resultFile, ios::trunc);
    outputFile<<"n,Ticks\n";
    outputFile.close();

    // Inititialize variables for testing
    int64_t maxArrayLength = 26200;
    int64_t minArrayLength = 700;
    int64_t arrayStepSize = 1000;
    int64_t numTestsToAvg = 100;

    int64_t currentArrayLength = minArrayLength;

    // Run tests
    for (currentArrayLength; currentArrayLength <= maxArrayLength; currentArrayLength += arrayStepSize){
        int64_t ticks = 0;

        for (int64_t test = 0; test<numTestsToAvg; test++){
            int *testArray = new int[currentArrayLength];
            fillArray(testArray, currentArrayLength);

            //Run test and return time taken to perform test
            ticks += (int64_t)findMedianTimeTest(testArray, currentArrayLength);
            delete testArray;
        }
        //Calculate average and output
        ticks = ticks/numTestsToAvg;
        outputResultToFile(currentArrayLength, ticks, resultFile);
    }
}

//Implentation of the algorithm with basic operations counter
int64_t findMedianCountTest(int testArray[], int64_t arrayLength){
    int64_t k;

    k = ceil(arrayLength/2.0);
    int64_t basicOperationsCounter = 0;

    for (int64_t i = 0; i<arrayLength; i++){
        int64_t numSmaller = 0;
        int64_t numEqual = 0;

        int64_t currentValue = testArray[i];


        for (int64_t j = 0; j<arrayLength; j++){
            basicOperationsCounter++;
            int64_t compareValue = testArray[j];

            if (compareValue < currentValue){
                numSmaller++;
            }else if(compareValue==currentValue){
                numEqual++;
            }
        }

        if (numSmaller<k&&k<=(numEqual+numSmaller)){
            i = arrayLength; // Set i to arrayLength to close the loop.
        }
    }
    return basicOperationsCounter;
}

//Implemented algorithm with timer
clock_t findMedianTimeTest(int testArray[], int64_t arrayLength){
    int64_t k;

    k = ceil(arrayLength/2.0);

    clock_t ticks = clock(); //Start timer
    for (int64_t i = 0; i<arrayLength; i++){
        int64_t numSmaller = 0;
        int64_t numEqual = 0;

        int64_t currentValue = testArray[i];

        for (int64_t j = 0; j<arrayLength; j++){
            int64_t compareValue = testArray[j];

            if (compareValue < currentValue){
                numSmaller++;
            }else if(compareValue==currentValue){
                numEqual++;
            }
        }
        if (numSmaller<k&&k<=(numEqual+numSmaller)){
            ticks = clock() - ticks; //End timer
            i = arrayLength;
        }
    }

    return ticks;
}

//Writes data to csv file
void outputResultToFile(int64_t arrayLength, int64_t outputData, char* resultFile){
    fstream outputFile;

    outputFile.open(resultFile, ios::app);

    outputFile << arrayLength << "," << outputData << "\n";
    outputFile.close();
}

//Implementation of algorithm for functional testing
void outputTestArray(){
    ofstream outputFile;
    int64_t arrayLength = 9;
    int64_t k;

    outputFile.open("sampleArray.csv", ios::trunc);

    for (int64_t x=0; x<10; x++){
    int * testArray = new int[arrayLength];
    fillArray(testArray, arrayLength);

    for (int64_t i=0; i<arrayLength; i++){
            outputFile << testArray[i] << "," << testArray[i] << "\n";
        }



    k = ceil(arrayLength/2.0);

    for (int64_t i = 0; i<arrayLength; i++){
        int64_t numSmaller = 0;
        int64_t numEqual = 0;

        int64_t currentValue = testArray[i];

        for (int64_t j = 0; j<arrayLength; j++){
            int64_t compareValue = testArray[j];

            if (compareValue < currentValue){
                numSmaller++;
            }else if(compareValue==currentValue){
                numEqual++;
            }
        }
        if (numSmaller<k&&k<=(numEqual+numSmaller)){
            outputFile << "Median," << currentValue << "\nIndex," << i << endl;
            i = arrayLength;
        }
    }
    }
}

//Random number generator to fill test array
void fillArray(int testArray[], int arrayLength){
    std::uniform_int_distribution<int> distribution(1, arrayLength*10);

    for (int currentIndex = 0; currentIndex < arrayLength; currentIndex++){
        int newValue = distribution(generator);
        testArray[currentIndex] = newValue;
    }

}

