#ifndef TESTARRAY_H
#define TESTARRAY_H
#include "randomTwister.h"

class TestArray{
    public:
        TestArray();
        TestArray(int arrayLength, RandomTwister twister);
        virtual ~TestArray();

        int getArrayLength() { return arrayLength; }
        int getElement(int index){ return testArray[index]; }
    protected:

    private:
        int *testArray;
        int arrayLength;
        void testFillArray(int *arrayToFill, RandomTwister twister);
};

#endif // TESTARRAY_H
