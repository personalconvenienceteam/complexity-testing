#ifndef ARRAYSET_H
#define ARRAYSET_H
#include "testArray.h"

class ArraySet
{
    public:
        ArraySet();
        virtual ~ArraySet();
        TestArray arraySet[TOTAL_ARRAYS];

    protected:

    private:
        const int MIN_ARRAY_SIZE = 10;
        const int MAX_ARRAY_SIZE = 100;
        const int ARRAY_STEP_SIZE = 10;
        const int TESTS_PER_SIZE = 50;
        const int TOTAL_ARRAYS = 4500;
};

#endif // ARRAYSET_H
