#ifndef MINDISTANCE_H
#define MINDISTANCE_H
#include "testArray.h"

void minDistance(TestArray **arraySet, int numberOfArrays, int numberOfTests);
int doMinOneCount(TestArray **arraySet, int index);
int doMinOneTime(TestArray **arraySet, int index);
#endif // MINDISTANCE_H
