#ifndef MINDISTANCE2_H
#define MINDISTANCE2_H
#include "testArray.h"

void minDistance2(TestArray **arraySet, int numberOfArrays, int numberOfTests);
int doMinTwoCount(TestArray **arraySet, int index);
int doMinTwoTime(TestArray **arraySet, int index);

#endif // MINDISTANCE2_H
