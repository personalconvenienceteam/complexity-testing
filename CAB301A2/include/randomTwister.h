#ifndef RANDOMTWISTER_H
#define RANDOMTWISTER_H


class RandomTwister
{
    public:
        RandomTwister();
        int randomInteger(int low, int high);
        void seedTwister();

    protected:

    private:

};

#endif // RANDOMTWISTER_H
