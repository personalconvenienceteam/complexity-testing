#ifndef FUNCTIONALTESTING_H
#define FUNCTIONALTESTING_H
#include <fstream>

void testFunction();
void runTests(int testNumber, int *testArray);
int findDmin1(int *testArray);
int findDmin2(int *testArray);

#endif
