#include <iostream>
#include "randomTwister.h"
#include "testArray.h"
#include "minDistance.h"
#include "minDistance2.h"
#include "functionalTesting.h"

using namespace std;
const int MIN_ARRAY_SIZE = 1000;
const int MAX_ARRAY_SIZE = 10000;
const int ARRAY_STEP_SIZE = 100;
const int TESTS_PER_SIZE = 50;
const int TOTAL_ARRAYS = 4500;

void testPrintArray(TestArray *arraySet[TESTS_PER_SIZE]);

int main(){

    testFunction();

    RandomTwister twister;
    TestArray *arraySet[TOTAL_ARRAYS];
    int currentArraySize = MIN_ARRAY_SIZE;

    for (int i = 0; i < TOTAL_ARRAYS; i+=TESTS_PER_SIZE){
        for (int j = i; j < i+TESTS_PER_SIZE; j++){
            arraySet[j] = new TestArray(currentArraySize,twister);
        }
        currentArraySize += ARRAY_STEP_SIZE;
    }

    minDistance(arraySet, TOTAL_ARRAYS, TESTS_PER_SIZE);
    minDistance2(arraySet, TOTAL_ARRAYS, TESTS_PER_SIZE);

    for (int i = 0; i < TOTAL_ARRAYS; i++){
        delete arraySet[i];
        arraySet[i]=NULL;
    }
    return 0;
}
