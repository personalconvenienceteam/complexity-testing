#include <iostream>
#include "randomTwister.h"

using namespace std;
const int MIN_ARRAY_SIZE = 10;
const int MAX_ARRAY_SIZE = 100;
const int ARRAY_STEP_SIZE = 10;
const int TESTS_PER_SIZE = 50;
const int TOTAL_ARRAYS = 4500;
RandomTwister twister;

void populateArray(int currentArray[], int arrayLength);
void printArray(int *arraySet[MAX_ARRAY_SIZE], int start, int endIndex, int currentArraySize);

int main(){
    twister.seedTwister();

    int *arraySet[TOTAL_ARRAYS];
    int currentArraySize = MIN_ARRAY_SIZE;

    for (int i = 0; i < TOTAL_ARRAYS; i += ARRAY_STEP_SIZE){
        for (int j = i; j < i + TESTS_PER_SIZE; j++){
            arraySet[j] = new int[currentArraySize];
            int tempArray[currentArraySize];
            populateArray(tempArray, currentArraySize);
            arraySet[j] = tempArray;
        }

        currentArraySize += ARRAY_STEP_SIZE;

    }

    currentArraySize = MIN_ARRAY_SIZE;

    for (int i=0; i<MAX_ARRAY_SIZE; i += ARRAY_STEP_SIZE){
        int start = i;
        int endIndex = i + ARRAY_STEP_SIZE;
        currentArraySize += i;
        printArray(arraySet, start, endIndex, currentArraySize);
    }

        return 0;

}

void populateArray(int currentArray[], int arrayLength){

    for (int i = 0; i < arrayLength; i++){
        currentArray[i] = twister.randomInteger(1, arrayLength);
    }

}

void printArray(int *arraySet[MAX_ARRAY_SIZE], int start, int endIndex, int currentArraySize){

    for (int i=start; i<endIndex; i++ ){
        for (int j = 0; j < currentArraySize; j++){
            cout<<arraySet[j][i]<< " : ";
        }
        cout<<endl;
    }
}


