#include "randomTwister.h"
#include <chrono>
#include <random>
#include <math.h>

std::mt19937 generator;

RandomTwister::RandomTwister(){
    seedTwister();
}

int RandomTwister::randomInteger(int low, int high) {
    std::uniform_int_distribution<int> distribution(low, high*10);
    int newValue = distribution(generator);

    return newValue;
}

void RandomTwister::seedTwister(){
    unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
    generator.seed(seed);
}

