#include <chrono>
#include <climits>
#include <iostream>
#include <fstream>
#include <stdlib.h>
#include "minDistance2.h"
#include "testArray.h"

using namespace std;
typedef chrono::high_resolution_clock Clock;

void minDistance2(TestArray **arraySet, int numberOfArrays, int numberOfTests){
    int opCount = 0;
    float averageTime = 0;


    ofstream opCountFile;
    opCountFile.open("AvgOpCountResult2.csv");
    opCountFile<<"Array Size, Average Operations,"<<endl;

    for (int i=0; i<numberOfArrays; i+=numberOfTests){
        opCount = doMinTwoCount(arraySet, i);
        int arraySize = arraySet[i]->getArrayLength();

        opCountFile<<arraySize<<","<<opCount<<","<<endl;
        cout<<"Array Size: "<<arraySize<<endl;
        cout<<"Average Operations Count: "<<opCount<<endl;

    }
    opCountFile.close();

    ofstream avTimeFile;
    avTimeFile.open("AvgTimeResult2.csv");
    avTimeFile<<"Array Size, Average Time(ns),"<<endl;

    for (int i=0; i<numberOfArrays; i+=numberOfTests){
        for (int j = i; j<i+numberOfTests; j++){
            float nanoSeconds=0;
            nanoSeconds = doMinTwoTime(arraySet, j);
            averageTime += nanoSeconds;
        }
        averageTime = averageTime/numberOfTests;
        int arraySize = arraySet[i]->getArrayLength();

        avTimeFile<<arraySize<<","<<averageTime<<","<<endl;
        cout<<"Average Time: "<<averageTime<<"ns"<<endl;
        cout<<"Array Size: "<<arraySize<<endl;

        averageTime = 0;
    }
    avTimeFile.close();


}

int doMinTwoCount(TestArray **arraySet, int index){
    int dmin = INT_MAX;
    int temp;
    int opCount = 0;
    int arrayLength = arraySet[index]->getArrayLength();

    for (int i = 0; i<arrayLength - 1; i++){

        for (int j = i + 1; j<arrayLength; j++){

            temp=abs(arraySet[index]->getElement(i)-arraySet[index]->getElement(j));
            opCount++;
            if (temp<dmin){
                dmin = abs(arraySet[index]->getElement(i)-arraySet[index]->getElement(j));

            }//end if

        }// end j for

    }// end i for
    return opCount;

}//end doOperationsCount

int doMinTwoTime(TestArray **arraySet, int index){
    int arrayLength = arraySet[index]->getArrayLength();
    int dmin = INT_MAX;
    int temp;
    int nanoSeconds;

    auto start= Clock::now();
    for (int i = 0; i<arrayLength; i++){

        for (int j = i + 1; j<arrayLength; j++){

            temp=abs(arraySet[index]->getElement(i)-arraySet[index]->getElement(j));
            if (temp<dmin){
                dmin = temp;

            }//end if
        }
    }
    auto endTimer = Clock::now();

    nanoSeconds=chrono::duration_cast<chrono::nanoseconds>(endTimer - start).count();
    return nanoSeconds;

}//end doOperationsCount


