#include "functionalTesting.h"
#include <fstream>
#include <assert.h>
#include <iostream>
#include <stdlib.h>
#include <climits>

using namespace std;

int TEST_ARRAY1[10]={1,2,3,4,5,6,7,8,9,10};
int TEST_ARRAY2[10]={1,11,22,34,47,61,76,92,109, 152};
int TEST_ARRAY3[10]={1,1,3,4,5,6,7,8,9,10};
int TEST_ARRAY4[10]={1,11,21,31,41,51,61,70,81,91};
int TEST_ARRAY5[10]={1,1000,2000,3000,4000,5000,6000,7000,8000,8998};
int TEST_ARRAY6[10]={100,90,80,70,60,50,40,30,20,95};
int TEST_ARRAY7[10]={1,5,9,3,13,17,21,25,29,33};
int TEST_ARRAY8[10]={100000,20,5000,3000,50000,40000,66000,90000,500,100010};
int TEST_ARRAY9[10]={30,60,90,120,150,180,190,220,250,280};
int TEST_ARRAY10[10]={66,77,88,99,44,55,33,22,11,72};
int DMINS[10]={1, 10, 0, 9, 998, 5, 2, 10, 10, 5};

void testFunction(){
    int testNumber = 1;
    ofstream fResult;
    fResult.open("FunctionalTestResults.csv");
    fResult<<"Test number,Expected dmin,Returned dmin,Passed,Array Elements"<<endl;
    fResult.close();
    runTests(testNumber, TEST_ARRAY1);
    testNumber++;
    runTests(testNumber, TEST_ARRAY2);
    testNumber++;
    runTests(testNumber, TEST_ARRAY3);
    testNumber++;
    runTests(testNumber, TEST_ARRAY4);
    testNumber++;
    runTests(testNumber, TEST_ARRAY5);
    testNumber++;
    runTests(testNumber, TEST_ARRAY6);
    testNumber++;
    runTests(testNumber, TEST_ARRAY7);
    testNumber++;
    runTests(testNumber, TEST_ARRAY8);
    testNumber++;
    runTests(testNumber, TEST_ARRAY9);
    testNumber++;
    runTests(testNumber, TEST_ARRAY10);

}

void runTests(int testNumber, int *testArray){
    int dmin;

    ofstream fResult;
    fResult.open("FunctionalTestResults.csv", ofstream::app);

    dmin=findDmin1(testArray);
    assert(("Dmin failed! Incorrect dmin returned!", dmin==DMINS[testNumber-1]));
    dmin=findDmin2(testArray);
    assert(("Dmin failed! Incorrect dmin returned!", dmin==DMINS[testNumber-1]));

    fResult<<testNumber<<","<<DMINS[testNumber-1]<<","<<dmin<<",TRUE,";

    for(int i=0; i<10; i++){
        fResult<<testArray[i]<<",";
    }

    fResult<<endl;
    fResult.close();

}

int findDmin1(int *testArray){
   int dmin = INT_MAX;
   for (int i = 0; i<10; i++){

        for (int j = 0; j<10; j++){
            if (i!=j&&(abs(testArray[i]-testArray[j])<dmin)){
                dmin = abs(testArray[i]-testArray[j]);
            }//end if
        }// end j for
    }// end i for
    return dmin;
}


int findDmin2(int *testArray){
    int dmin = INT_MAX;
    int temp;

    for (int i = 0; i<9; i++){
        for (int j = i + 1; j<10; j++){
            temp=abs(testArray[i]-testArray[j]);
            if (temp<dmin){
                dmin = temp;
            }//end if
        }// end j for
    }// end i for
    return dmin;
}
