#include "testArray.h"
#include "randomTwister.h"

TestArray::TestArray(){}

TestArray::TestArray(int arrayLength, RandomTwister twister){
    this->arrayLength = arrayLength;
    testArray = new int[arrayLength];

    testFillArray(testArray, twister);
}

void TestArray::testFillArray(int *arrayToFill, RandomTwister twister){
    for (int i = 0; i < this->arrayLength; i++){
        arrayToFill[i] = twister.randomInteger(1, this->arrayLength);
    }
}

TestArray::~TestArray()
{
    //dtor
}
