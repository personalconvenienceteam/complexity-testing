#include "arraySet.h"
#include "randomTwister.h"
#include <iostream>

const int MIN_ARRAY_SIZE = 10;
const int MAX_ARRAY_SIZE = 100;
const int ARRAY_STEP_SIZE = 10;
const int TESTS_PER_SIZE = 50;
const int TOTAL_ARRAYS = 4500;

RandomTwister twister;
int **arraySet;

ArraySet::ArraySet()
{
//    int **arraySet;
    int currentArraySize = MIN_ARRAY_SIZE;

    arraySet = new int *[TOTAL_ARRAYS];

    for (int i = 0; i < TOTAL_ARRAYS; i += ARRAY_STEP_SIZE){

        for (int j = i; j < i + TESTS_PER_SIZE; j++){
            arraySet[j] = new int[currentArraySize];
            int tempArray[currentArraySize];
            populateArray(tempArray, currentArraySize);
            arraySet[j] = tempArray;
        }

        currentArraySize += ARRAY_STEP_SIZE;

    }

    currentArraySize = MIN_ARRAY_SIZE;
    int arrayCount = 0;

}

void ArraySet::populateArray(int currentArray[], int arrayLength){

    for (int i = 0; i < arrayLength; i++){
        currentArray[i] = twister.randomInteger(1, arrayLength);
    }

}

int ArraySet::getIndex(int i, int j){
    return arraySet[i][j];
}

int ArraySet::getArraySize(int arrayIndex){
    int arraySize;
    arraySize = sizeof(arraySet);

    return arraySize;
}
ArraySet::~ArraySet()
{
    //dtor
}
