#include <chrono>
#include <climits>
#include <iostream>
#include <fstream>
#include <stdlib.h>
#include "minDistance.h"
#include "testArray.h"

using namespace std;
typedef chrono::high_resolution_clock Clock;

void minDistance(TestArray **arraySet, int numberOfArrays, int numberOfTests){
    int opCount = 0;
    float averageTime = 0;


    ofstream opCountFile;
    opCountFile.open("AvgOpCountResult1.csv");
    opCountFile<<"Array Size, Average Operations,"<<endl;

    for (int i=0; i<numberOfArrays; i+=numberOfTests){
//        for (int j = i; j<i+numberOfTests; j++){
            opCount = doMinOneCount(arraySet, i);
//        }
        int arraySize = arraySet[i]->getArrayLength();
        opCountFile<<arraySize<<","<<opCount<<","<<endl;
        cout<<"Array Size: "<<arraySize<<endl;
        cout<<"Average Operations Count: "<<opCount<<endl;
    }
    opCountFile.close();

    ofstream avTimeFile;
    avTimeFile.open("AvgTimeResult1.csv");
    avTimeFile<<"Array Size, Average Time(ns),"<<endl;

    for (int i=0; i<numberOfArrays; i+=numberOfTests){
        for (int j = i; j<i+numberOfTests; j++){
            float nanoSeconds=0;
            nanoSeconds = doMinOneTime(arraySet, j);
            averageTime += nanoSeconds;
        }
        averageTime = averageTime/numberOfTests;
        int arraySize = arraySet[i]->getArrayLength();

        avTimeFile<<arraySize<<","<<averageTime<<","<<endl;
        cout<<"Average Time: "<<averageTime<<"ns"<<endl;
        cout<<"Array Size: "<<arraySize<<endl;

        averageTime = 0;
    }
    avTimeFile.close();


}

int doMinOneCount(TestArray **arraySet, int index){
    int dmin = INT_MAX;
    int opCount = 0;
    int arrayLength = arraySet[index]->getArrayLength();

    for (int i = 0; i<arrayLength; i++){

        for (int j = 0; j<arrayLength; j++){

            if (i!=j){
                opCount++;
                if(abs(arraySet[index]->getElement(i)-arraySet[index]->getElement(j))<dmin){
                dmin = abs(arraySet[index]->getElement(i)-arraySet[index]->getElement(j));
                }
            }//end if

        }// end j for

    }// end i for
    return opCount;

}//end doOperationsCount

int doMinOneTime(TestArray **arraySet, int index){
    int arrayLength = arraySet[index]->getArrayLength();
    int dmin = INT_MAX;
    int nanoSeconds;

    auto start= Clock::now();
    for (int i = 0; i<arrayLength; i++){

        for (int j = 0; j<arrayLength; j++){

            if (i!=j&&abs(arraySet[index]->getElement(i)-arraySet[index]->getElement(j))<dmin){
                dmin = abs(arraySet[index]->getElement(i)-arraySet[index]->getElement(j));

            }//end if

        }// end j for

    }// end i for
    auto endTimer = Clock::now();

    nanoSeconds=chrono::duration_cast<chrono::nanoseconds>(endTimer - start).count();
    return nanoSeconds;

}//end doOperationsCount

